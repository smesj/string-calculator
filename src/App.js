import React, { useState } from 'react';
import './App.css';
import TextField from '@material-ui/core/TextField'
import { Formik, Form } from 'formik';
import Button from '@material-ui/core/Button'
import { Typography, SnackbarContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const App = () => {

	const add = (input, actions) => {
		const inputDelim = input.total.match(/[^//].*(?=\\n)/g);
		let inputArr = []

		if (inputDelim) {
			const stringStartIndex = input.total.lastIndexOf("\\n");
			const inputString = input.total.substring(stringStartIndex+2);
			inputArr = inputString.split(inputDelim[0]);
		} else {
			inputArr = input.total.replace(/\\n/g, "").split(',');
		}

		try {
			calulateTotal(inputArr, actions, inputDelim);
		} catch(e) {
			console.log(e)
			setError(true);
			setErrorMessage(e.toString());
		}		
	}

	const calulateTotal = (inputArr, actions, delimiter) => {
		let total = 0;
		const negatives = [];
		const processedArr = [];

		inputArr.map((number) => {
			let currNum = parseInt(number, 10);

			if (isNaN(currNum)) currNum = 0;

			if (Math.sign(currNum) === -1){
				processedArr.push(currNum);
				negatives.push(currNum);
			} else if (!(currNum > 1000)){
				processedArr.push(currNum);
				total = total + currNum;
			}	

			return true;
		})

		if (negatives.length > 0) {
			actions.setSubmitting(false);
			setCalcHistory([...calcHistory, {inputArr: processedArr, total: 'Error: negative numbers used', delimiter}])
			throw new Error(`Negative numbers not allowed (${negatives.toString()})`)
		};

		setCalcHistory([...calcHistory, {inputArr: processedArr, total, delimiter}])

		actions.setSubmitting(false);

		return total;
	}

	const handleClose = () => {
		setError(false);
		setErrorMessage('');
	}
 
	const [calcHistory, setCalcHistory] = useState([])
	const [error, setError] = useState(false);
	const [errorMessage, setErrorMessage] = useState('');
	const classes = useStyles();

  	return (
		<>
			<Snackbar
				anchorOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
				open={error}
				autoHideDuration={6000}
				onClose={handleClose}
			>
				<SnackbarContent
					message={<span>{errorMessage}</span>}
					className={classes.error}
					action={[
						<IconButton
						key="close"
						color="inherit"
						onClick={handleClose}
						>
							<CloseIcon />
						</IconButton>,
					]}
				/>
			</Snackbar>	  
			<div className={classes.app}>
				<div className={classes.input}>
					<Typography variant='h4'>String Calculator</Typography>
					<Formik
						initialValues={{
							total: 0
						}}
						onSubmit={(values, actions) => {
							add(values, actions)
						}}
						render={({ errors, status, touched, isSubmitting, values, handleChange }) => (
							<div>
								<Form style={{display:'flex', flexDirection:'column'}}>
									<TextField
										label='Enter a string to calcluate'
										placeholder="1,2,3 or use custom delimiter //$\n1$2$3"
										name='total'
										onChange={handleChange}
									/>
									<Button type="submit" variant='contained' disabled={isSubmitting}>
										Submit
									</Button>
								</Form>
							</div>
						)}
					/>
				</div>
				<div className={classes.history}>
					<Typography variant='h4' style={{marginBottom: 8}}>Calculation Output</Typography>
					<Card>
						<CardContent>
							{calcHistory.map((entry, i) => (
								<div key={i} style={{paddingTop: 8, paddingBottom: 8}}>
									<Typography>{entry.inputArr.toString().replace(/,/g, " + ")} = {entry.total}</Typography>
									{entry.delimiter && (
										<Typography>Delimiter: {entry.delimiter}</Typography>
									)}
								</div>							
							))}
						</CardContent>
					</Card>
				</div>
			</div>
		</>
  	);
}

export default App;

const useStyles = makeStyles(theme => ({
	app: {
		display: 'flex',
		flexDirection: 'row',
		padding: 64,
	},
	input: {
		width: '50%',
		padding: 64
	},
	history: {
		width: '50%',
		padding: 64
	},
	error: {
		backgroundColor: theme.palette.error.dark,
	},
}));
